public class Glass {

    private float maxGallons;
    private float currentGallons = 0;

    public Glass(float maxGallons) {

        this.maxGallons = maxGallons;

    }

    public void addWater(float gallonsToAdd) throws GlassOverflownException {

        float newLevel = this.currentGallons + gallonsToAdd;

        if (newLevel > maxGallons) {
            this.currentGallons = this.maxGallons;
            throw new GlassOverflownException("Glass capacity exceeded;" + " you have spilt " + (newLevel - this.maxGallons) + " gallons.");
        } else {
            this.currentGallons = newLevel;
        }
    }
}
